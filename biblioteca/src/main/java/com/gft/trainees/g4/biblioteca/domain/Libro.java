package com.gft.trainees.g4.biblioteca.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity(name = "LibroNuevo")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Libro {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO )
	private Long idLibro;
	@Column
	private String tituloLibro;
	@Column
	private String autor;
	@Column
	private Integer anio;
	@Column
	private String editorial;
}
