package com.gft.trainees.g4.biblioteca.v1.controller;

import java.util.List;

import com.gft.trainees.g4.biblioteca.v1.controller.dto.DTOLibro;
import com.gft.trainees.g4.biblioteca.v1.controller.dto.DTORespuesta;

public interface ILibroController {
	
	public DTOLibro nuevoLibro(DTOLibro dtoLibro);
	public DTOLibro consultaLibro(Long id);
	public DTOLibro actualizaLibro(Long id, DTOLibro dtoLibro);
	public DTORespuesta eliminaLibro(Long id);
	public List<DTOLibro> consultaTodosLibros();
	

}
