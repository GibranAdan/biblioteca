package com.gft.trainees.g4.biblioteca.mapper;

import org.mapstruct.Mapper;

import com.gft.trainees.g4.biblioteca.domain.Respuesta;
import com.gft.trainees.g4.biblioteca.v1.controller.dto.DTORespuesta;

@Mapper(componentModel = "spring")
public interface IRespuestaMapper {
	
	DTORespuesta respuestaToDTORespuesta(Respuesta respuesta);
	Respuesta dtoRepuestaToRespuesta(DTORespuesta dtoRespuesta);
}
