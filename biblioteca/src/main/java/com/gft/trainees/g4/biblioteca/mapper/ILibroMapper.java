package com.gft.trainees.g4.biblioteca.mapper;

import org.mapstruct.Mapper;

import com.gft.trainees.g4.biblioteca.domain.Libro;
import com.gft.trainees.g4.biblioteca.v1.controller.dto.DTOLibro;

@Mapper(componentModel = "spring")
public interface ILibroMapper {
	 Libro dtoLibroToLibro(DTOLibro dtoLibro);
	 DTOLibro LibroToDTOLibro(Libro Libro);
}
