package com.gft.trainees.g4.biblioteca.v1.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gft.trainees.g4.biblioteca.mapper.ILibroMapper;
import com.gft.trainees.g4.biblioteca.mapper.IRespuestaMapper;
import com.gft.trainees.g4.biblioteca.service.ILibroService;
import com.gft.trainees.g4.biblioteca.v1.controller.dto.DTOLibro;
import com.gft.trainees.g4.biblioteca.v1.controller.dto.DTORespuesta;

@RestController
@RequestMapping("gft/Libro/v1")
public class LibroController implements ILibroController{
	
	@Autowired
	private ILibroService libroService;
	
	@Autowired
	private ILibroMapper iLibroMapper;
	
	@Autowired
	private IRespuestaMapper iRespuestaMapper;
	
	@PostMapping
	public DTOLibro nuevoLibro(@RequestBody DTOLibro dtoLibro) {
		DTOLibro dtoLibroCreado = null;
		if(dtoLibro !=null) {
			dtoLibroCreado = iLibroMapper.LibroToDTOLibro(
							libroService.nuevoLibro(
							iLibroMapper.dtoLibroToLibro(dtoLibro)));
		}
		return dtoLibroCreado;
	}

	@Override
	@GetMapping(path="/{id}")
	public DTOLibro consultaLibro(@PathVariable("id") Long id) {
		DTOLibro dtoLibroConsulta = null;
		if(id != null && id > 0) {
			dtoLibroConsulta = iLibroMapper.LibroToDTOLibro(libroService.consultaLibro(id));	
		}
		return dtoLibroConsulta;
	}

	@PutMapping(path = "/{id}")
	public DTOLibro actualizaLibro(@PathVariable("id") Long id, @RequestBody DTOLibro dtolibro) {
		DTOLibro dtoLibroActual = null;
		if(id != null && id > 0)
		{
			dtoLibroActual = iLibroMapper.LibroToDTOLibro(libroService.actualizaLibro(id, iLibroMapper.dtoLibroToLibro(dtolibro)));
		}
		return dtoLibroActual;
	}

	@Override
	@DeleteMapping(path="/{id}")
	public DTORespuesta eliminaLibro(@PathVariable("id") Long id) {
		DTORespuesta dtoRespuesta = null;
		if(id != null && id > 0) {
			dtoRespuesta = iRespuestaMapper.respuestaToDTORespuesta(libroService.eliminaLibro(id));	
		}
		return dtoRespuesta;
	}

	@Override
	@GetMapping
	public List<DTOLibro> consultaTodosLibros() {

		List<DTOLibro> listaDTOLibro = 
				libroService.consultaTodosLibros().
				stream().map(x -> iLibroMapper.LibroToDTOLibro(x)).
				collect(Collectors.toList());
		return listaDTOLibro;
	}

}
