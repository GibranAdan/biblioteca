package com.gft.trainees.g4.biblioteca.dao.repository;

import org.springframework.data.repository.CrudRepository;

import com.gft.trainees.g4.biblioteca.domain.Libro;

public interface ILibroRepository extends CrudRepository<Libro, Long>{

}
