package com.gft.trainees.g4.biblioteca.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.gft.trainees.g4.biblioteca.dao.repository.ILibroRepository;
import com.gft.trainees.g4.biblioteca.domain.Libro;
import com.gft.trainees.g4.biblioteca.domain.Respuesta;

@Service
public class LibroService implements ILibroService{
	
	@Autowired
	private ILibroRepository libroRepository;
	
	@Override
	public Libro nuevoLibro(Libro libro) {
		Libro libroNuevo = null;
		if(libro != null) {
			libroNuevo = libroRepository.save(libro);
		}
		return libroNuevo;
	}

	@Override
	public Libro consultaLibro(Long id) {
		Optional<Libro> libro=null;
		if(id != null && id >0) {
		libro = libroRepository.findById(id);
		}
		return libro != null && libro.isPresent()?libro.get():null;
	}

	@Override
	public Libro actualizaLibro(Long id, Libro libro) {
		Libro libroActualizado = null;
		if(id != null && id >0 && libroRepository.existsById(id)) {
			libroActualizado = libroRepository.findById(id).get();
			libroActualizado.setAnio(libro.getAnio()!=null?libro.getAnio():libroActualizado.getAnio());
			libroActualizado.setAutor(StringUtils.isEmpty(libro.getAutor())?libro.getAutor():libroActualizado.getAutor());
			libroActualizado.setEditorial(libro.getEditorial()!=null?libro.getEditorial():libroActualizado.getEditorial());
			libroActualizado.setTituloLibro(StringUtils.isEmpty(libro.getTituloLibro())?libro.getTituloLibro():libroActualizado.getTituloLibro());
			libroActualizado = libroRepository.save(libroActualizado);
		}
		return libroActualizado;
	}
	

	@Override
	public Respuesta eliminaLibro(Long id) {
		Respuesta respuesta = new Respuesta();
		if(id != null && id >0 && libroRepository.existsById(id)) {
			libroRepository.deleteById(id);
			respuesta.setMensaje("Libro eliminado");
		}else {
			respuesta.setMensaje("El libro no existe");
		}
		return respuesta;
	}

	@Override
	public List<Libro> consultaTodosLibros() { 
		Iterable<Libro> iterableLibro = libroRepository.findAll();
		List<Libro> listaLibros = (List<Libro>)iterableLibro;
		return listaLibros;
	}



}
