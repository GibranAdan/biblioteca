package com.gft.trainees.g4.biblioteca.v1.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DTORespuesta {

	private String mensaje;
}
