package com.gft.trainees.g4.biblioteca.service;

import java.util.List;

import com.gft.trainees.g4.biblioteca.domain.Libro;
import com.gft.trainees.g4.biblioteca.domain.Respuesta;


public interface ILibroService {
	
	public Libro nuevoLibro(Libro libro);
	public Libro consultaLibro(Long id);
	public Libro actualizaLibro(Long id, Libro libro);
	public Respuesta eliminaLibro(Long id);
	public List<Libro> consultaTodosLibros();
	

}
