package com.gft.trainees.g4.biblioteca.v1.controller;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.gft.trainees.g4.biblioteca.domain.Libro;
import com.gft.trainees.g4.biblioteca.mapper.ILibroMapper;
import com.gft.trainees.g4.biblioteca.mapper.IRespuestaMapper;
import com.gft.trainees.g4.biblioteca.service.ILibroService;
import com.gft.trainees.g4.biblioteca.v1.controller.dto.DTOLibro;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.atLeastOnce;


@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class LibroControllerTest {
	
	@InjectMocks
	private LibroController libroController;
	
	@Mock
	private ILibroService libroService;
	
	@Mock
	private ILibroMapper iLibroMapper;
	
	@Mock
	private IRespuestaMapper iRespuestaMapper;
	
	@Test
	public void testNuevoLibroHappyPath() {
		
		Libro libro = new Libro(1L,"Libro","Gibran",2009,"Santillana");
		DTOLibro dtoLibro = new DTOLibro("Libro",1L,"Gibran",2009,"Santillana");
	
		//Preparar escenario, observar las inyeccions
		Mockito.when(iLibroMapper.LibroToDTOLibro(libro)).thenReturn(dtoLibro);
		Mockito.when(libroService.nuevoLibro(libro)).thenReturn(libro);
		Mockito.when(iLibroMapper.dtoLibroToLibro(dtoLibro)).thenReturn(libro);
		
		//Ejecutar prueba
		DTOLibro dtoLibroRespuesta = libroController.nuevoLibro(dtoLibro);
		
		//Verificar ejecucion de metodos en el metodo a probar
		verify(iLibroMapper,atLeastOnce()).LibroToDTOLibro((Libro)Mockito.anyObject());
		verify(libroService,atLeastOnce()).nuevoLibro((Libro)Mockito.anyObject());
		verify(iLibroMapper,atLeastOnce()).dtoLibroToLibro((DTOLibro)Mockito.anyObject());
		
		//Validar resultado
		Assert.assertNotNull(dtoLibroRespuesta);
		
	}

	
}
